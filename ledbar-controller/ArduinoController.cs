﻿using System;
using System.IO.Ports;
using System.Threading;
namespace ledbar_controller
{
	public class ArduinoController
	{
		string Port { get; set; }
		SerialPort Serial { get; set; }

		public ArduinoController(string port)
		{
			if (port == null)
				throw new System.ArgumentNullException("Port can't be null");

			Port = port;

			try
			{
				Serial = new SerialPort(Port, 9600);
			}
			catch (System.IO.IOException e)
			{
				Console.WriteLine("Can't connect to the serial port: " + e.Message);
				throw e;
			}
		}

		public void LightsOn()
		{
			Console.WriteLine("Lights on");
			if (!Serial.IsOpen)
			{
				try
				{
					Serial.Open();
					
				}
				catch (System.Exception e)
				{
					Console.WriteLine(e.Message);
					return;
				}
			}
			Serial.Write("a");
		}

		public void LightsOff()
		{
			Console.WriteLine("Lights off");
			if (!Serial.IsOpen)
			{
				try
				{
					Serial.Open();
					
				}
				catch (System.Exception e)
				{
					Console.WriteLine(e.Message);
					return;
				}
			}
			Serial.Write("b");
		}
	}

}


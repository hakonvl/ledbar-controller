﻿using System;

namespace ledbar_controller
{
	internal class ControllerApp

	{
		private static ArduinoController Controller;

		private static void Main(string[] args)
		{
			Controller = new ArduinoController("COM3");

			if (args.Length == 0)
				Console.WriteLine("Usage: ledbar-controller.exe on/off");
			else if (args[0] == "on")
				Controller.LightsOn();
			else if (args[0] == "off")
				Controller.LightsOff();
			else
				Console.WriteLine("Unknown parameter " + args[0] + ". Usage: ledbar-controller.exe on/off");
		}
	}
}
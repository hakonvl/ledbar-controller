// include the SoftwareSerial library so you can use its functions:
#include <SoftwareSerial.h>

#define rxPin 10
#define txPin 11
#define ledPin 13
// set up a new serial port
SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);
bool state = false;
void setup()  {
  // define pin modes for tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  // set the data rate for the SoftwareSerial port
  //mySerial.begin(9600);
  Serial.begin(9600); 
}

void loop() {
  if (Serial.available()>0)
  {
       digitalWrite(ledPin, LOW);
       Serial.print(Serial.read());

 }
 if(state)
   digitalWrite(ledPin, HIGH );
 else
    digitalWrite(ledPin, LOW);
  
   digitalWrite(ledPin, HIGH);   // sets the LED on
  delay(1000);                  // waits for a second
  digitalWrite(ledPin, LOW);    // sets the LED off
  delay(1000); 
  Serial.print("asd");
}

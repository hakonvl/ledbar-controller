#include <Adafruit_NeoPixel.h>

#ifdef __AVR__
  #include <avr/power.h>
#endif

#define rxPin 10
#define txPin 11
// set up a new serial port
//SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);

#define ledPin 6
#define dPin 13 //LED for debug
#define NUM_LEDS 60

#define BRIGHTNESS_DARK 0
#define BRIGHTNESS_LIGHT 255

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, ledPin, NEO_GRB + NEO_KHZ800);

void setup() {
   // define pin modes for tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  pinMode(dPin, OUTPUT);
  // set the data rate for the SoftwareSerial port
  Serial.begin(9600);

  strip.setBrightness(BRIGHTNESS_DARK);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void updateState()
{
    int b = Serial.read();    
    if(b == 97)
    {
      strip.setBrightness(BRIGHTNESS_LIGHT);
      strip.show();
      digitalWrite(dPin, HIGH); 
    }
    else if(b == 98)
    {
      strip.setBrightness(BRIGHTNESS_DARK);
      strip.show();
      digitalWrite(dPin, LOW);
    }
}

void loop()
{
	updateState();
	rainbow(20);
}

const float colorSpeed = 0.1f;
float colorIteration = 0;

void rainbow(uint16_t wait)
{
	const float L = 0.5f;
	const float a = 0.5f;
	
	const float P = NUM_LEDS / 2;
	const float k = 2 * PI / P;
	
	const float F = P / 3;
	
	for (uint8_t i = 0; i < strip.numPixels(); i++)
	{
		float r = L + a * sin(k * (colorIteration + i - 0 * F));
		float g = L + a * sin(k * (colorIteration + i - 1 * F));
		float b = L + a * sin(k * (colorIteration + i - 2 * F));
		
		strip.setPixelColor(i, r * 0xFF, g * 0xFF, b * 0xFF);
	}
	
	colorIteration += colorSpeed;
	
    strip.show();
    delay(wait);
}

uint32_t colors[] = {0xff0000, 0xff5500, 0xffaa00, 0xffff00, 0xaaff00, 0x55ff00, 0x00ff00, 0x00ff55, 0x00ffaa, 0x00ffff, 0x00aaff, 0x0055ff, 0x0000ff, 0x5500ff, 0xaa00ff, 0xff00ff, 0xff00aa, 0xff0055};
uint16_t colorI = 0;

void coolRainbowCool(uint16_t wait)
{
	size_t colorArraySize = sizeof(colors) / sizeof(uint32_t);
	for (uint8_t i = 0; i < strip.numPixels(); i++)
	{
		strip.setPixelColor(i, colors[(colorI + i) % colorArraySize]);
	}
	
	colorI++;
    strip.show();
    delay(wait);
}
